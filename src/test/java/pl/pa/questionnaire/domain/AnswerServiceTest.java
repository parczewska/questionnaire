package pl.pa.questionnaire.domain;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.pa.questionnaire.*;
import pl.pa.questionnaire.model.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class AnswerServiceTest {

    private static AnswerRepository answerRepository = new AnswerRepositoryInMemory();
    private static QuestionRepository questionRepository = new QuestionRepositoryInMemory();
    private static AnswerService answerService = new AnswerService(answerRepository, questionRepository);
    private static QuestionService questionService = new QuestionService(questionRepository);

    @BeforeAll
    static void init() {
        QuestionDTO openQuestion = QuestionDTO.builder().questionContent("Ile masz lat?").
                questionType(QuestionType.OPEN).
                possibleAnswers(Collections.EMPTY_LIST).build();
        questionService.add(openQuestion);

        QuestionDTO closedQuestion = QuestionDTO.builder().questionContent("Czy masz psa?").
                questionType(QuestionType.CLOSED).
                possibleAnswers(List.of(PossibleAnswerDTO.builder().answerContent("tak").answerType(AnswerType.SINGLE_CHOICE).build(),
                        PossibleAnswerDTO.builder().answerContent("nie").answerType(AnswerType.SINGLE_CHOICE).build())).build();
        questionService.add(closedQuestion);
    }

    @Test
    void shouldAddAnswerToQuestion() {
        //given
        AnswerDTO answerDTO = AnswerDTO.builder().answerContent("45").build();
        answerService.add(0, answerDTO);
        boolean exist = true;
        //when
        Optional<Answer> result = answerRepository.findById(0);
        //then
        assertEquals(exist, result.isPresent());
    }

    @Test
    void shouldNotAddAnswerToNotExistQuestion() {
        //given
        AnswerDTO answerDTO = AnswerDTO.builder().answerContent("100").build();
        //then
        assertThrows(InvalidQuestionException.class, () -> {
            answerService.add(22, answerDTO);
        });
    }

    @Test
    void shouldNotAddInvalidAnswerToClosedQuestion() {
        //given
        AnswerDTO answerDTO = AnswerDTO.builder().answerContent("mam kota").build();
        //then
        assertThrows(InvalidAnswerException.class, () -> {
            answerService.add(1, answerDTO);
        });
    }
}
