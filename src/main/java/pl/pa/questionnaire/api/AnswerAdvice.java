package pl.pa.questionnaire.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.pa.questionnaire.InvalidAnswerException;

@RestControllerAdvice
public class AnswerAdvice {

    @ExceptionHandler(InvalidAnswerException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String invalidQuestionId(InvalidAnswerException invalidAnswerException) {

        return invalidAnswerException.getMessage();
    }
}
