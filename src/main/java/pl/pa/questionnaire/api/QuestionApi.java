package pl.pa.questionnaire.api;

import org.springframework.web.bind.annotation.*;
import pl.pa.questionnaire.QuestionService;
import pl.pa.questionnaire.model.QuestionDTO;

@RestController
public class QuestionApi {

    QuestionService questionService;

    public QuestionApi(QuestionService questionService) {
        this.questionService = questionService;
    }

    @RequestMapping(value = "/questions", method = RequestMethod.POST)
    public QuestionDTO add(@RequestBody QuestionDTO questionDTO) {
        return questionService.add(questionDTO);
    }

    @RequestMapping(value = "/questions/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable long id) {
        questionService.delete(id);
    }
}
