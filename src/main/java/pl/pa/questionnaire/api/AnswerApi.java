package pl.pa.questionnaire.api;

import org.springframework.web.bind.annotation.*;
import pl.pa.questionnaire.AnswerService;
import pl.pa.questionnaire.model.AnswerDTO;

@RestController
public class AnswerApi {

    AnswerService answerService;

    public AnswerApi(AnswerService answerService) {
        this.answerService = answerService;
    }

    @RequestMapping(value = "/answer/{questionId}", method = RequestMethod.POST)
    public AnswerDTO add(@PathVariable long questionId, @RequestBody AnswerDTO answerDTO) {
        return answerService.add(questionId, answerDTO);
    }

    @RequestMapping(value = "/answer/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable long id) {
        answerService.delete(id);
    }
}
