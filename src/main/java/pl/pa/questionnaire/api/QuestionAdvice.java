package pl.pa.questionnaire.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.pa.questionnaire.InvalidQuestionException;

@RestControllerAdvice
public class QuestionAdvice {

    @ExceptionHandler(InvalidQuestionException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    String invalidQuestionId(InvalidQuestionException invalidQuestionIdException) {

        return invalidQuestionIdException.getMessage();
    }
}
