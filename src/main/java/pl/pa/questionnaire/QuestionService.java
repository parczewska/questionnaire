package pl.pa.questionnaire;

import org.springframework.stereotype.Service;
import pl.pa.questionnaire.model.*;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionService {

    QuestionRepository questionRepository;

    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public QuestionDTO add(QuestionDTO questionDTO) {

        List<PossibleAnswer> possibleAnswers = PossibleAnswerDTOMapping.map(questionDTO.getPossibleAnswers());

        Question question = new Question(questionDTO.getQuestionContent(), questionDTO.getQuestionType(), possibleAnswers);
        questionRepository.save(question);
        return questionDTO;
    }

    public void delete(long id) {

        Optional<Question> result = questionRepository.findById(id);

        if (result.isPresent()) {
            questionRepository.delete(result.get());
        } else throw new InvalidQuestionException("Nie istnieje pytanie o podanym Id.");
    }
}
