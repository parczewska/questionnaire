package pl.pa.questionnaire;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pa.questionnaire.model.Question;


import java.util.Optional;
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

    Optional<Question> findById(long id);
}