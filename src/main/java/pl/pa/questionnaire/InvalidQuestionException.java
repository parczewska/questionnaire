package pl.pa.questionnaire;

public class InvalidQuestionException extends RuntimeException {

    public InvalidQuestionException(String message) {

        super(message);
    }
}
