package pl.pa.questionnaire;

import org.springframework.stereotype.Service;
import pl.pa.questionnaire.model.Answer;
import pl.pa.questionnaire.model.AnswerDTO;
import pl.pa.questionnaire.model.Question;

import java.util.Optional;

@Service
public class AnswerService {

    AnswerRepository answerRepository;
    QuestionRepository questionRepository;

    public AnswerService(AnswerRepository answerRepository, QuestionRepository questionRepository) {
        this.answerRepository = answerRepository;
        this.questionRepository = questionRepository;
    }

    public AnswerDTO add(long questionId, AnswerDTO answerDTO) {

        Optional<Question> result = questionRepository.findById(questionId);

        if (result.isPresent()) {

            Question question = result.get();
            Answer answer = new Answer();
            answer.setAnswerContent(answerDTO.getAnswerContent());

            if (question.isPossibleAnswer(answer)) {

                answerRepository.save(answer);
                question.addAnswer(answer);
                questionRepository.save(question);
                return answerDTO;
            }
            throw new InvalidAnswerException("Podana odpowiedź nie jest dostępna.");
        }
        throw new InvalidQuestionException("Nie można dodać odpowiedzi do pytania, ponieważ nie istnieje pytanie o podanym Id.");
    }

    public void delete(long id) {

        Optional<Answer> result = answerRepository.findById(id);

        if (result.isPresent()) {
            answerRepository.delete(result.get());
        } else throw new InvalidAnswerException("Nie istnieje odpowiedź o takim Id.");
    }
}
