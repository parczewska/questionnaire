package pl.pa.questionnaire.model;

public enum AnswerType {

    SINGLE_CHOICE,
    MULTIPLE_CHOICE,
}
