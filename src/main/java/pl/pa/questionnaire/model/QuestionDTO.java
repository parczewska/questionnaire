package pl.pa.questionnaire.model;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class QuestionDTO {

    @JsonProperty
    private String questionContent;
    @JsonProperty
    private QuestionType questionType;
    @JsonProperty
    private List<PossibleAnswerDTO> possibleAnswers;

    public String getQuestionContent() {
        return questionContent;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public List<PossibleAnswerDTO> getPossibleAnswers() {
        return possibleAnswers;
    }
}
