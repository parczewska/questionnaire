package pl.pa.questionnaire.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class PossibleAnswerDTO {

    @JsonProperty
    private String answerContent;
    @JsonProperty
    private AnswerType answerType;

    public String getAnswerContent() {
        return answerContent;
    }

    public AnswerType getAnswerType() {
        return answerType;
    }
}
