package pl.pa.questionnaire.model;

import java.util.ArrayList;
import java.util.List;

public class PossibleAnswerDTOMapping {

    public static List<PossibleAnswer> map(List<PossibleAnswerDTO> possibleAnswersDTO) {

        List<PossibleAnswer> possibleAnswers = new ArrayList<>();

        for (PossibleAnswerDTO possibleAnswerDTO : possibleAnswersDTO) {

            PossibleAnswer possibleAnswer = new PossibleAnswer();
            possibleAnswer.setAnswerContent(possibleAnswerDTO.getAnswerContent());
            possibleAnswer.setAnswerType(possibleAnswerDTO.getAnswerType());

            possibleAnswers.add(possibleAnswer);
        }
        return possibleAnswers;
    }
}
