package pl.pa.questionnaire.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;

@Builder
public class AnswerDTO {

    @JsonProperty
    private String answerContent;

    public String getAnswerContent() {
        return answerContent;
    }
}
