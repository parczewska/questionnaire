package pl.pa.questionnaire.model;

import java.util.ArrayList;
import java.util.List;

public class AnswerMapping {

    public static List<AnswerDTO> map(List<Answer> answers) {

        List<AnswerDTO> answersDTO = new ArrayList<>();

        for (Answer answer : answers) {

            AnswerDTO answerDTO = AnswerDTO.builder().
                    answerContent(answer.getAnswerContent()).build();

            answersDTO.add(answerDTO);
        }
        return answersDTO;
    }
}
