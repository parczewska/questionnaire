package pl.pa.questionnaire.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "question_content")
    private String questionContent;
    @Column(name = "question_type")
    @Enumerated(EnumType.STRING)
    private QuestionType questionType;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "question_answer_id")
    private List<Answer> answers;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "question_id")
    private List<PossibleAnswer> possibleAnswers;

    public Question() {
    }

    public Question(String questionContent, QuestionType questionType, List<PossibleAnswer> possibleAnswers) {
        this.questionContent = questionContent;
        this.questionType = questionType;
        this.answers = new ArrayList<>();
        this.possibleAnswers = possibleAnswers;
    }

    public void addAnswer(Answer answer) {
        answers.add(answer);
    }

    public boolean isPossibleAnswer(Answer answer) {

        if (!possibleAnswers.isEmpty()) {
            return possibleAnswers.stream().anyMatch(e -> e.getAnswerContent().equals(answer.getAnswerContent()));
        }
        return true;
    }

    public long getId() {
        return id;
    }
}
