package pl.pa.questionnaire.model;

import javax.persistence.*;

@Entity
@Table(name = "answer")
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "answer_content")
    private String answerContent;

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }

    public long getId() {
        return id;
    }

    public String getAnswerContent() {
        return answerContent;
    }
}
