package pl.pa.questionnaire.model;

import javax.persistence.*;

@Entity
@Table(name = "possible_answers")
public class PossibleAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "answer_content")
    private String answerContent;
    @Column(name = "answer_type")
    @Enumerated(EnumType.STRING)
    private AnswerType answerType;

    void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }

    public void setAnswerType(AnswerType answerType) {
        this.answerType = answerType;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public AnswerType getAnswerType() {
        return answerType;
    }
}
